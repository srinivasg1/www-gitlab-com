###
#
# Release post data file
#
# Start the release post with this file, named `YYYY_MM_22_gitlab_X_Y_released.yml`
# placed into `data/release_posts/`.
#
# Notes:
#  - All description entries support markdown. Use it as you do for a regular markdown file.
#   Just make sure the indentation is respected.
#
## Optional entries:
#
# - Features
#   - Top:
#     - documentation_text: "Read through the documentation on Amazing Feature" # text displaying the documentation_link
#   - Primary:
#     - documentation_link: 'https://docs.gitlab.com/#amazing' # webpage or documentation
#     - documentation_text: "Learn more about Amazing Feature" # text displaying the documentation_link when link != documentation
#     - video: # overrides image_url
#   - Secondary, upgrade barometer, deprecations:
#     - documentation_link: 'https://docs.gitlab.com/#amazing' # webpage or documentation
#     - documentation_text: "Learn more about Amazing Feature" # text displaying the documentation_link
#     - image_url: '/images/x_y/feature-a.png'
#   - Optional to all features:
#     - image_noshadow: true # this eliminates double shadows for images that already have a shadow
#
# Read through the Release Posts Handbook for more information:
# https://about.gitlab.com/handbook/marketing/blog/release-posts/
#
###

features:
# TOP FEATURE
  top:
    - name: Amazing Feature
      available_in: [ees, eep] # required
      documentation_link: 'https://docs.gitlab.com/#amazing' # webpage or documentation - required
      documentation_text: "Learn more about Amazing Feature" # optional
      description: | # supports markdown
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid laudantium,
        quisquam pariatur architecto quo vel sequi, aperiam ex autem itaque saepe
        sint dignissimos, quis voluptates similique.
        Hic veritatis facere eligendi, minus nisi eveniet delectus fugiat.

        <figure class="video_container">
          <iframe src="https://www.youtube.com/embed/videoid" frameborder="0" allowfullscreen="true"> </iframe>
        </figure>

        Lorem ipsum dolor sit amet, `consectetur adipisicing` elit.
        Rerum nisi et ex rem, obcaecati, commodi incidunt fugit,
        deleniti nesciunt aperiam consequuntur.

        1. Lorem ipsum dolor.
        1. Lorem ipsum dolor sit.

        > Lorem ipsum dolor sit amet.

        Lorem ipsum dolor sit amet, [consectetur adipisicing elit](#link).
        Quae repellat, at ullam amet.

        ![Amazing feature screenshot](/images/X_Y/amazing-feature.png){:.shadow}

        Lorem ipsum dolor sit amet, consectetur adipisicing elit.

# PRIMARY FEATURES
  primary:
    - name: Lorem ipsum
      available_in: [ce, ees, eep] # required
      documentation_link: '/features/lorem-ipsum' # webpage or documentation - optional
      documentation_text: "Learn more about Lorem ipsum" # optional
      image_url: '/images/9_X/feature-a.png' # required
      image_noshadow: true # optional
      video: 'https://www.youtube.com/embed/enMumwvLAug' # example - optional - overrides image_url
      description: | # supports markdown
        Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
        Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.

    - name: Lorem
      available_in: [ees, eep] # required
      documentation_link: 'https://docs.gitlab.com/#lorem' # webpage or documentation - optional
      documentation_text: "Read through the documentaion on Lorem" # optional
      image_url: '/images/9_X/feature-a.png' # required
      description: | # supports markdown
        Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
        Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.

# SECONDARY FEATURES
  secondary:
    - name: Lorem ipsum
      available_in: [eep] # required
      documentation_link: 'https://docs.gitlab.com/#'
      documentation_text: "Learn more on how to lorem ipsum." # webpage or documentation - optional
      image_url: '/images/9_X/feature-a.png' # optional
      image_noshadow: true # optional
      description: | # supports markdown
        Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
        Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.

        Lorem ipsum dolor sit amet, consectetur adipisicing elit.
        Voluptate eveniet, adipisci earum sed harum nostrum
        itaque beatae, repellat sunt unde.

    - name: Sunt nam accusantium nostrum
      available_in: [ees, eep] # required
      documentation_link: 'https://docs.gitlab.com/#' # webpage or documentation - optional
      documentation_text: "Praesentium tempore nulla asperiores" # optional
      description: | # supports markdown
        Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
        Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.

    - name: Omnibus Improvements
      available_in: [ce, ees, eep] # required
      documentation_link:  # webpage or documentation - optional
      description: | # supports markdown
        Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
        Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.

    - name: Performance Improvements
      available_in: [ce, ees, eep] # required
      documentation_link:  # webpage or documentation - optional
      description: | # supports markdown
        Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.
        Perferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.

# EXTRAS (Optional)
# extras:
#  - title: "Hello World"
#    description: | # supports markdown
#     Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequuntur, beatae!

# MVP
mvp:
  fullname: # Name Surname
  gitlab: # gitlab.com username
  description: | # example (supports markdown)
    Dosuken extended our [Pipelines API](http://docs.gitlab.com/ce/api/pipelines.html#list-project-pipelines)
    by [adding additional search attributes](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/9367).
    This is a huge improvement to our CI API, for example enabling queries to easily return the latest
    pipeline for a specific branch, as well as a host of other possibilities. Dosuken also made a great
    contribution last release, laying the foundation for
    [scheduled pipelines](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/10133). Thanks Dosuken!

# COVER IMAGE LICENCE
cover_img:
  image_url: 'https://example.com#link_to_original_image' # required
  licence: CC0 # which licence the image is available with - required
  licence_url: '#https://example.com#link_to_licence' # required

# CTA BUTTONS (optional)
cta:
  - title: "Join us for an upcoming event" # default
    link: '/events/'
  - title: "Join the release webcast" # optional
    link: 
  - title: Lorem ipsum amet # optional
    link:

# UPGRADE BAROMETER
barometer:
  description: | # example (supports markdown)
    To upgrade to GitLab 9.2, no downtime is required.

    However we're also migrating data for CI jobs. If you have a significant number of jobs, this could take some time.

    Starting with GitLab 9.1.0 it's possible to upgrade to a newer version of GitLab without having to take your GitLab instance offline. However, for this to work there are the following requirements:

    1.  You can only upgrade 1 release at a time. For example, if 9.1.15 is the last release of 9.1 then you can safely upgrade from that version to 9.2.0. However, if you are running 9.1.14 you first need to upgrade to 9.1.15.
    2.  You have to use [post-deployment migrations](https://docs.gitlab.com/ce/development/post_deployment_migrations.html).
    3.  You are using PostgreSQL. If you are using MySQL you will still need downtime when upgrading.

    This applies to major, minor, and patch releases unless stated otherwise in a release post.

    A new version of our API was released in [GitLab 9.0](https://about.gitlab.com/2017/03/22/gitlab-9-0-released/#api-v4). While existing calls to API v3 will continue to work until August 2017, we advise you to make any necessary changes to applications that use the v3 API. [Read the documentation](https://docs.gitlab.com/ee/api/v3_to_v4.html) to learn more.

# DEPRECATIONS (optional)
# include as many deprecation blocks as necessary
deprecations:
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017. # example
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
  - feature_name: Lorem ipsum dolor
    due: May 22nd, 2017.
    description: |  # example (supports markdown)
      Lorem ipsum dolor sit amet, consectetur adipisicing elit.
      Veritatis, quisquam.
