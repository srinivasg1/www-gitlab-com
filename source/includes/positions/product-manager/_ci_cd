:markdown

  ## Product Manager, CI/CD Pipelines

  We're looking for product managers that can help us work on the future of
  developer tools. Specifically, building out continuous integration (CI), continuous
  delivery (CD), and beyond.

  We believe developers deserve an intuitive, integrated solution that covers the
  entire software development lifecycle. We believe running tests is just the
  beginning of automation. We believe deploying your code should be automated and
  repeatable, and most of all, easy. We believe in pushing the boundaries of
  best-practices, and bringing these to every developer, so that doing the right
  thing is also the easy, default way of working.

  As Product Manager, CI/CD Pipelines, you will find the weak spots of GitLab CI
  and execute plans to improve them. You will have a strong understanding of CI/CD and
  an understanding of deployment infrastructure and container technologies.
